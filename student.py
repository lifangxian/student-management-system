from flask import Flask, render_template, request, redirect, url_for
import re
# 由于使用flask框架需要实例化一个
web = Flask(__name__)
# 创建一个学生表的数据库
students = [{'son': '101', 'name': 'Tom', 'age': 18, 'sex': '男',
             'class_room': '1班', 'phone': '13788888888',
             'address': '北京市海淀区'},
            {'son': '102', 'name': 'Jerry', 'age': 19, 'sex': '男',
             'class_room': '2班', 'phone': '13899999999',
             'address': '北京市朝阳区'}]

@web.route('/')
def index():

    return render_template('student.html', students=students)

def is_valid_phone(phone):
    '''验证手机号是否合法'''
    pattern = re.compile(r'^\d{11}$')
    return bool(pattern.match(phone))

# 添加学生信息
@web.route("/add_stu", methods=["get", "post"])
def add_student():
    # 如果请求方式是post请求通过表单来获取到学生信息
    print(request.method)
    if request.method == "POST":

        son = request.form.get('son')
        name = request.form.get('name')
        age = request.form.get('age')
        sex = request.form.get('sex')
        class_room = request.form.get('class_room')
        # phone = request.form.get('phone')
        if is_valid_phone(request.form.get('phone')):
            phone = request.form.get('phone')
        address = request.form.get('address')
        #定义一个student学生动态变量
        student = {'son': son, 'name': name, 'age': age, 'sex': sex, 'class_room': class_room, 'phone': phone,
                   'address': address}
        # student = {son: 'son', name: 'name', age: 'age', sex: 'sex', class_room: 'class_room', phone: 'phone',
        #            address: 'address'}

        students.append(student)#往列表里添加数据
        print(students)
        # 如果是get请求则通过redirect重定向url_for，添加完成直接返回到首页
        return redirect(url_for('index'))
        # return render_template('student.html',students=students)

    return render_template('add_stu.html')


# 删除学生信息
@web.route('/<son>', methods=['get', 'post'])
def delect_student(son):
    if request.method == "GET":
        for stu in students:
            if stu['son'] == son:
                students.remove(stu)
                break
    return redirect(url_for('index'))


# 修改学生信息
@web.route('/update', methods=['get', 'post'])
def ubdate_student():
    if request.method == "GET":
        son = request.args.get('son')

        student_update = []
        print(type(student_update))
        for s in students:
            if s['son'] == son:
                # a=s.update
                print(type(s))
                student_update = s
                print(type(student_update))

        return render_template('update_stu.html', student=student_update)
    else:
        xuesheng = request.form.to_dict()
        print(xuesheng)
        for s in students:
            if s['son'] == xuesheng['son'] and is_valid_phone(request.form.get('phone')):
                students.remove(s)
                students.append(xuesheng)
        print(students)
        return render_template("student.html", students=students)


# 查询学生
@web.route('/', methods=["post"])
def sel_student():
    if request.method == "POST":
        name = request.form.get('name', '')
        print(type(name))
        if name:
            #
            filtered_students = [s for s in students if s['name'].find(name) >= 0]
            print(type(filtered_students))
            return render_template('student.html', students=filtered_students if filtered_students else None)

        else:
            return render_template('student.html', students=students)


if __name__ == '__main__':
    web.run(host="0.0.0.0", port=5555, debug=True)
