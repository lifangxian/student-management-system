from typing import Dict, Union
import requests
from flask import Flask, request, jsonify, render_template

app = Flask(__name__)

students = [
    {
        'id': 1,
        'name': '张三',
        'age': 18,
        'sex': '男',
        'class': '一班',
        'phone': '1234567890',
        'address': '北京市海淀区'
    },
    {
        'id': 2,
        'name': '李四',
        'age': 19,
        'sex': '女',
        'class': '二班',
        'phone': '2345678901',
        'address': '北京市朝阳区'
    },
    {
        'id': 3,
        'name': '王五',
        'age': 20,
        'sex': '男',
        'class': '三班',
        'phone': '3456789012',
        'address': '北京市东城区'
    }
]


# 定义首页路由
@app.route('/', methods=['GET'])
def index():
    print(students)  # 打印出学生信息
    return render_template('index.html', students=students)


# 定义增加学生信息页面路由
@app.route('/add', methods=['GET', 'POST'])
def add_st():
    print(request.method)
    if request.method == 'POST':
        student = request.form
        student_info = {
            'id': student['id'],
            'name': student['name'],
            'age': student['age'],
            'sex': student['sex'],
            'class': student['class'],
            'phone': student['phone'],
            'address': student['address']
        }
        students.append(student_info)
        print(f'新增的学生列表{student_info}')
        return render_template('index.html', students=students)
    else:
        print('是get请求')
        return render_template('add_st.html')