from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

# 使用列表存储学生信息
# students = []
#
# # 获取所有学生信息
# def get_students():
#     students.sort(key=lambda x: x['sno'])
#     return students
#
# # 根据学号查询学生信息
# def get_student(sno):
#     for student in students:
#         if student['sno'] == sno:
#             return student
#     return None
#
# # 增加学生信息
# def add_student(sno, name, age, gender, _class, tel, address):
#     student = {'sno': sno, 'name': name, 'age': age, 'gender': gender, 'class': _class, 'tel': tel, 'address': address}
#     students.append(student)
#
# # 修改学生信息
# def update_student(sno, name, age, gender, _class, tel, address):
#     for student in students:
#         if student['sno'] == sno:
#             student['name'] = name
#             student['age'] = age
#             student['gender'] = gender
#             student['class'] = _class
#             student['tel'] = tel
#             student['address'] = address
#             break
#
# # 删除学生信息
# def delete_student(sno):
#     for student in students:
#         if student['sno'] == sno:
#             students.remove(student)
#             break

# 显示学生信息列表页面
@app.route('/')
def show_students():
    # students = get_students()
    return render_template('student.html')

# 显示增加学生信息页面
# @app.route('/add_student', methods=['GET', 'POST'])
# def add_student_page():
#     if request.method == 'GET':
#         return render_template('add_student.html')
#     else:
#         sno = request.form['sno']
#         name = request.form['name']
#         age = request.form['age']
#         gender = request.form['gender']
#         _class = request.form['class']
#         tel = request.form['tel']
#         address = request.form['address']
#         add_student(sno, name, age, gender, _class, tel, address)
#         return redirect(url_for('show_students'))
#
# # 显示修改学生信息页面
# @app.route('/update_student/<string:sno>', methods=['GET', 'POST'])
# def update_student_page(sno):
#     if request.method == 'GET':
#         student = get_student(sno)
#         return render_template('update_student.html', student=student)
#     else:
#         sno = request.form['sno']
#         name = request.form['name']
#         age = request.form['age']
#         gender = request.form['gender']
#         _class = request.form['class']
#         tel = request.form['tel']
#         address = request.form['address']
#         update_student(sno, name, age, gender, _class, tel, address)
#         return redirect(url_for('show_students'))

# 删除学生信息
# @app.route('/delete_student/<string:sno>')
# def delete_student_page(sno):
#     delete_student(sno)
#     return redirect(url_for('show_students'))



if __name__ == '__main__':
    app.run(host="0.0.0.0",port=6666,debug=True)